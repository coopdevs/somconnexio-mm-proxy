LOGIN_ERROR_MSG = "Contacteu siusplau amb l'equip de sistemes:\n{}"
INCOMPLETE_ACCOUNT_MSG = "Falten dades per a obrir un compte a MM:\n{}"
INCOMPLETE_ORDER_ITEM_MSG = "Falten dades per demanar un contracte de mòbil a MM:\n{}"

PROVINCE_NOT_FOUND_MSG = (
    "El codi {} no s'ha trobat a la taula de províncies amb "
    "codificació ISO 3166-2:ES"
)
TARIF_NOT_FOUND_MSG = (
    "El codi {} no existeix a la nostra base de dades de tarifes de mòbil"
)
ONE_SHOT_ADDITIONAL_BOND_NOT_FOUND_MSG = (
    "El codi {} no existeix a la nostra base de dades "
    "d'abonaments addicionals 'one shot` de mòbil"
)
INTERNATIONAL_TARIF_NOT_FOUND_MSG = (
    "La tarifa amb {} minuts no existeix a la nostra "
    "base de dades de tarifes internacionals de MM."
)
INVALID_PC_MSG = (
    "El format de codi postal no és vàlid: {}. Ha de ser un codi de 5 dígits"
)
PHONE_NUMBER_WITHOUT_PORTABILITY_MSG = (
    "S'ha enviat un telèfon mòbil: {} en una petició "
    "d'alta nova. Només s'accepten números de telèfon "
    "en casos de portabilitat."
)
PORTABILITY_WITHOUT_PHONE_NUMBER_MSG = (
    "S'ha enviat una portabilitat sense cap número de telèfon mòbil."
)

CANCELLED_ORDER_ITEM_MSG = (
    "L'ordre amb id: {} ha estat cancel·lada per MásMovil.\n"
    "L'ordre ja no es pot reintentar i se n'haurà de crear una de nova."
)

ERROR_ORDER_ITEM_MSG = (
    "L'ordre amb id: {} conté un error i ha estat rebutjada per MásMovil.\n"
    "Per a més informació sobre el problema cal consultar el Community.\n"
    "La ordre es pot reintentar des d'allà corregint les dades, "
    "i llavors aquest tiquet s'haurà d'avançar manualment a 'Introduit a la Plataforma'."
)

ORDER_ITEM_SUCCESS_MSG = (
    "OrderItem creat a MM amb id:\t{}\nCorresponent a l'account amb id:\t{}"
)

ONE_SHOT_BOND_ADDED_MSG = (
    "Abonament addicional registrat a la plataforma de MM.\n "
    "Asset id: {}\nCodi de l'abonament extra: {}.\n "
    "Canvi ja efectiu, activat amb data: {}, i vàl·lid fins a final de mes"
)

TARIFF_CHANGED_MSG = (
    "Canvi de tarifa correctament realitzat a la plataforma de MM.\n "
    "Asset id: {}\nCodi de la nova tarifa: {}.\n "
    "Data d'inici efectiu del canvi: {}"
)

NULL_CHANGE_TARIFF_REQUEST = (
    "El canvi de tarifa és nul.\n "
    "La tarifa nova correspon a la vella en el community de MM"
)

EMPTY_ORDER_ITEM_ID_TO_CHECK_REQUEST = (
    "El paràmetre 'MMOrderItemID' no pot ser buit.\n "
    "Contacta amb el nostre equip de sistemes"
)
