from api.tariff import Tariff
from api.models import NewLineRequest, DonorOperator
from api.exceptions import (NewNumberRegistrationWithPhoneNumberException,
                            PortabilityWithoutPhoneNumberException)
from api.services.vat import VAT


class NewLineRequestMapping:
    def __init__(self, ticket_data, account, ticket_id=""):
        """
        Build an object with the attributes required to create an OrderItem
        in the MM API. Fill these attributes with OTRS data.
        If no new owner for the requested portability is given, we assign the line
        ownership to the same account party.
        """

        if ticket_data.get("mobile_service_type") == "altaNova" and \
                ticket_data.get("phone_number"):
            raise NewNumberRegistrationWithPhoneNumberException(
                ticket_id, ticket_data.get("phone_number")
            )
        elif ticket_data.get("mobile_service_type") == "portabilitat" and not \
                ticket_data.get("phone_number"):
            raise PortabilityWithoutPhoneNumberException(ticket_id)

        new_line_request_entry = NewLineRequest.objects.create()

        tariff = Tariff(
            ticket_data.get("product_code") or None,
            ticket_data.get("international_minutes") or None,
        )
        productInfo = {
            "id": tariff.mm_tariff,
            "additionalBonds": tariff.additional_bonds,
        }
        if tariff.promotions:
            productInfo["promotions"] = tariff.promotions

        # Unique identifier of 18 characters that the "cableoperador"
        # must send with sequential numbers
        self.transactionId = new_line_request_entry.get_transaction_id()

        docid = VAT(ticket_data.get("docid") or account.documentNumber)

        self.lineInfo = [
            {
                "idLine": new_line_request_entry.id,
                "docid": docid.remove_VAT_format(),
                "phoneNumber": ticket_data.get("phone_number") or None,
                "iccid": ticket_data.get("ICC_SC") or None,
                "iccid_donante": ticket_data.get("donor_ICC") or None,
                "productInfo": productInfo,
            }
        ]

        if ticket_data.get("donor_operator"):
            self.lineInfo[0]["donorOperator"] = DonorOperator.search_by_OTRS_code(
                ticket_data.get("donor_operator")
            ).mm_code

        self._complete_with_customer_type_information(ticket_data, account, docid)

    def _complete_with_customer_type_information(self, ticket_data, account, docid):
        """
        Fill NewLineRequest with attributes that depend on who the client is:
        either a particular or an organization
        """

        if docid.contains_CIF():
            doc_type = "CIF"
        elif docid.contains_NIE():
            doc_type = "NIE"
        else:
            doc_type = "NIF"

        if doc_type == "CIF":
            party_information = {
                "documentType": doc_type,
                "corporateName": ticket_data.get("name") or account.corporateName,
            }
        else:
            party_information = {
                "documentType": doc_type,
                "name": ticket_data.get("name") or account.name,
                "surname": ticket_data.get("surname") or account.surname,
            }
        self.lineInfo[0].update(party_information)
