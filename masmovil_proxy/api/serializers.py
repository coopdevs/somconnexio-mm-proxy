from rest_framework import serializers


class AccountSerializer(serializers.Serializer):
    name = serializers.CharField()
    town = serializers.CharField()
    surname = serializers.CharField()
    stair = serializers.CharField()
    roadType = serializers.CharField()
    roadNumber = serializers.CharField()
    roadName = serializers.CharField()
    region = serializers.CharField()
    province = serializers.CharField()
    postalCode = serializers.CharField()
    phone = serializers.CharField()
    id = serializers.CharField()
    flat = serializers.CharField()
    email = serializers.EmailField()
    door = serializers.CharField()
    documentType = serializers.CharField()
    documentNumber = serializers.CharField()
    corporateName = serializers.CharField()
    buildingPortal = serializers.CharField()


class HTTPErrorSerializer(serializers.Serializer):
    status_code = serializers.CharField()
    message = serializers.CharField()
    fields = serializers.CharField()
