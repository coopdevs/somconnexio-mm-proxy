from datetime import datetime, date
from dateutil.relativedelta import relativedelta


class Date:
    OTRS_format = "%Y-%m-%d %H:%M:%S"
    MM_format = "%Y-%m-%d"

    @classmethod
    def format_date_from_OTRS_to_MM(cls, otrs_date):
        """
        Recieves a string date from OTRS with format "%Y-%m-%d %H:%M:%S"
        Returns a string date correctly formatted to send to MM
        """
        date = datetime.strptime(otrs_date, cls.OTRS_format)
        return date.strftime(cls.MM_format)

    @classmethod
    def get_first_day_of_next_month(cls):
        """
        Computes the first day of the next month from today.
        Returns a string formatted date of the computed day.
        """
        today = date.today()
        first_day = today.replace(day=1) + relativedelta(months=1)

        return first_day.strftime(cls.MM_format)
