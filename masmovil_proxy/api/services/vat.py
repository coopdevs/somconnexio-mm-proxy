class VAT:
    def __init__(self, vat):
        if vat.startswith("ES"):
            self.vat = vat
        else:
            self.vat = "ES" + vat

    def contains_NIE(self):
        """
        Check if vat contains a NIE
        returns boolean
        """
        return True if self.vat[2] in ["X", "Y", "Z"] else False

    def contains_CIF(self):
        """
        Check if vat contains a CIF
        returns boolean
        """
        if self.vat[2].isalpha() and not self.contains_NIE() and len(self.vat) == 11:
            return True
        else:
            return False

    def remove_VAT_format(self):
        """
        If we recieved an spanish formatted VAT, remove the first two characters
        ('ES') to return a NIF (CIF, DNI) or NIE formatted documentNumber
        """
        return self.vat[2:]
