import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from api.tasks import change_tariff

logger = logging.getLogger(__name__)


class ChangeTariffView(APIView):
    def patch(self, request):

        request_data = request.data

        logger.info(
            "Tariff change request recieved. Request body: {}".format(request_data)
        )

        change_tariff.apply_async(
            kwargs={"request_body": request_data}, queue="mm-proxy"
        )

        return Response()
