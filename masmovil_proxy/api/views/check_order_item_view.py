import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from api.tasks import check_order_item

logger = logging.getLogger(__name__)


class CheckOrderItemView(APIView):
    def post(self, request):

        request_data = request.data

        logger.info(
            "Order item checking request recieved. Request body: {}".format(
                request_data
            )
        )

        check_order_item.apply_async(
            kwargs={"request_body": request_data}, queue="mm-proxy"
        )

        return Response()
