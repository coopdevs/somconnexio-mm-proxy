from __future__ import absolute_import, unicode_literals

import time
from celery.decorators import task
from celery.utils.log import get_task_logger
from bugsnag.celery import connect_failure_handler
from pymasmovil.models.session import Session
from pymasmovil.errors.exceptions import (
    MissingLoginCredentialsError,
    AutenthicationError,
)

from api.account import Account
from api.order_item import OrderItem
from api.asset import Asset
from api.services.date import Date
from api.OTRS_catalan_messages import (
    LOGIN_ERROR_MSG,
    CANCELLED_ORDER_ITEM_MSG,
    ERROR_ORDER_ITEM_MSG,
    ORDER_ITEM_SUCCESS_MSG,
    EMPTY_ORDER_ITEM_ID_TO_CHECK_REQUEST
)
from otrs.services.update_ticket_with_error import UpdateTicketWithError
from otrs.services.update_ticket_with_provisioning_info import (
    UpdateTicketWithProvisioningInfo,
)

logger = get_task_logger(__name__)
connect_failure_handler()


@task()
def create_order_item(request_body):
    ticket_id = request_body["ticket_id"]
    customer_id = request_body["customer_user"]
    session = get_MM_session(ticket_id)

    # Get account from MM
    account = Account(
        session, request_body["ticket_account_data"], customer_id, ticket_id
    ).get_or_create()

    # MM team suggested to add a delay after creating an account and before creating
    # an order-item on top of it.
    # The order-item creation fails if the account has not yet been integrated with
    # their THOR server (our OpenCell), which is why we need the timelapse.
    # If the order-item creation fails, a rollback applies and the new account is not
    # created.
    time.sleep(10)

    # Create order-item in that MM account
    order_item = OrderItem(session, ticket_id).create(
        account, request_body["ticket_order_item_data"]
    )

    UpdateTicketWithProvisioningInfo(
        ticket_id,
        order_item_created_msg=ORDER_ITEM_SUCCESS_MSG.format(order_item.id, account.id),
        mm_order_item_id=order_item.id, phone_number=order_item.phone
    ).run()


@task()
def check_order_item(request_body):

    ticket_id = request_body["ticket_id"]
    order_item_id = request_body["mm_order_item_id"]
    billing_start_date = request_body["activate_date"]

    if not order_item_id:
        UpdateTicketWithError(
            ticket_id,
            {"missatge": EMPTY_ORDER_ITEM_ID_TO_CHECK_REQUEST},
            "resolució",
            "OrderItem",
        ).run()
        return

    session = get_MM_session(ticket_id)

    order_item = OrderItem(session, ticket_id).get(order_item_id)

    if order_item.status == "Completada":
        phone = order_item.phone

        asset = Asset(session, ticket_id).get_by_phone(phone)

        UpdateTicketWithProvisioningInfo(
            ticket_id, asset=asset, billing_date=billing_start_date
        ).run()

    elif order_item.status == "Error":
        # TODO: Big refactor of exception rasing to notify OTRS!
        UpdateTicketWithError(
            ticket_id,
            {"missatge": ERROR_ORDER_ITEM_MSG.format(order_item_id)},
            "processement",
            "OrderItem",
        ).run()

    elif order_item.status == "Cancelada":
        UpdateTicketWithError(
            ticket_id,
            {"missatge": CANCELLED_ORDER_ITEM_MSG.format(order_item_id)},
            "resolució",
            "OrderItem",
        ).run()


@task()
def change_tariff(request_body):

    ticket_id = request_body["ticket_id"]
    phone = request_body["phone_number"]
    product_code = request_body["product_code"]
    change_date = request_body["change_date"]

    formatted_change_date = change_date and Date.format_date_from_OTRS_to_MM(change_date) or ""

    logger.info(
        "Tariff change request recieved.\n"
        + "Ticket id: {}, phone number: {}, change_date: {}".format(
            ticket_id, phone, formatted_change_date
        )
    )

    session = get_MM_session(ticket_id)

    asset_service = Asset(session, ticket_id)
    asset = asset_service.get_by_phone(phone)
    asset_service.change_tariff(asset, product_code, formatted_change_date)


@task()
def add_one_shot_bonds(request_body):

    ticket_id = request_body["ticket_id"]
    phone = request_body["phone_number"]
    product_code = request_body["product_code"]

    session = get_MM_session(ticket_id)

    asset_service = Asset(session, ticket_id)
    asset = asset_service.get_by_phone(phone)
    asset_service.add_one_shot_additional_bond(asset, product_code)


def get_MM_session(ticket_id):
    """
    Create a MM session (requires MM API credentials) and send it to every MM endpoint call
    to prove authentication
    """
    try:
        session = Session.create()
    except (MissingLoginCredentialsError, AutenthicationError) as exception:
        UpdateTicketWithError(
            ticket_id,
            {"missatge": LOGIN_ERROR_MSG.format(exception.message)},
            "creació",
            "Sessió",
        ).run()
        raise exception
    return session
