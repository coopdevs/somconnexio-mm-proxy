from celery.utils.log import get_task_logger
from django.db import connections
from datetime import datetime
from bugsnag.celery import connect_failure_handler
from pymasmovil.models.asset import Asset as MMAsset
from pymasmovil.errors.http_error import HTTPError
from pymasmovil.errors.exceptions import UnknownMMError

from otrs.services.update_ticket_with_error import UpdateTicketWithError
from otrs.services.update_ticket_with_provisioning_info import (
    UpdateTicketWithProvisioningInfo,
)

from api.tariff import Tariff
from api.models import (NewLineRequest, OneShotAdditionalBonds,
                        Tariffs, TRVProducts)
from api.exceptions import (
    OneShotAdditionalBondNotFoundException,
    TariffNotFoundException,
    NullTariffChangeException
)
from api.OTRS_catalan_messages import ONE_SHOT_BOND_ADDED_MSG, TARIFF_CHANGED_MSG
from api.services.date import Date

logger = get_task_logger(__name__)
connect_failure_handler()


class Asset:
    def __init__(self, session, ticket_id):
        self.session = session
        self.ticket_id = ticket_id

    def get_by_phone(self, phone):
        """
        Recover the account from which the order-item has been completed and get
        its resulting asset searching by phone
        """

        # Get the new asset assigned to the account
        try:
            asset = MMAsset.get_by_phone(self.session, phone)
        except HTTPError as error:
            UpdateTicketWithError(
                self.ticket_id, error.__dict__, "obtenció", "Asset"
            ).run()
            raise error

        logger.info("Asset correctly recieved with id: {}".format(asset.id))

        return asset

    def change_tariff(self, asset, product_code, change_date=None):
        """
        Change current contracted mobile tariff of a given asset
        """
        try:
            tariff = Tariff(product_code)
        except TariffNotFoundException as exception:
            UpdateTicketWithError(
                self.ticket_id,
                {"missatge": exception.message},
                "canvi de tarifa",
                "línia mòbil",
            ).run()
            raise exception

        # If change date is not defined, take the first day from next month as default
        execute_date = change_date or Date.get_first_day_of_next_month()

        update_product_params = self._build_change_tariff_params(asset, tariff,
                                                                 execute_date)

        try:
            MMAsset.update_product(self.session, **update_product_params)
        except HTTPError as error:
            UpdateTicketWithError(
                self.ticket_id, error.__dict__, "canvi de tarifa", "línia mòbil"
            ).run()
            raise error
        except UnknownMMError as exception:
            UpdateTicketWithError(
                self.ticket_id,
                {"missatge": exception.message},
                "canvi de tarifa",
                "línia mòbil",
            ).run()
            raise exception

        UpdateTicketWithProvisioningInfo(
            self.ticket_id,
            tariff_change_msg=TARIFF_CHANGED_MSG.format(
                asset.id, product_code, execute_date
            ),
        ).run()

        logger.info(
            "Tariff change request accepted. "
            + "Asset id: {}, productMobile: {}, start_date: {}".format(
                asset.id, product_code, execute_date
            )
        )

    def add_one_shot_additional_bond(self, asset, product_code):

        new_line_request_entry = NewLineRequest.objects.create()
        transaction_id = new_line_request_entry.get_transaction_id()

        try:
            one_shot_additional_bond = OneShotAdditionalBonds.search_by_product_code(
                product_code
            )
        except (OneShotAdditionalBondNotFoundException) as exception:
            UpdateTicketWithError(
                self.ticket_id,
                {"missatge": exception.message},
                "addició",
                "abonament one-shot addicional",
            ).run()
            raise exception

        new_trv_bond = False
        additional_bond = {"id": one_shot_additional_bond.additional_bond, "qty": 1}
        update_product_params = {
            "asset_id": asset.id,
            "transaction_id": transaction_id,
            "additional_bonds": [additional_bond],
        }
        # If the current tariff has a TRV product, we must remove its TRV bond
        # and add a new TRV one
        if asset.productId in TRVProducts.get_product_codes():
            additional_bond = {"id": one_shot_additional_bond.trv_bond, "qty": 1}
            # So far, all TRV tariffs (all tariffs in fact) have a single additionalBond.
            # Whenever this fails to be true, this code will need to be updated.
            update_product_params.update({"additional_bonds": [additional_bond]})

            if not asset.additionalBonds:
                new_trv_bond = True
                update_product_params.update({"bonds_to_remove": []})

            else:
                new_trv_bond = bool(
                    asset.additionalBonds[0]["id"] != one_shot_additional_bond.trv_bond)
                update_product_params.update({
                    "bonds_to_remove": [asset.additionalBonds[0]["AssetId"]],
                })

        try:
            MMAsset.update_product(self.session, **update_product_params)
        except UnknownMMError as exception:
            UpdateTicketWithError(
                self.ticket_id,
                {"missatge": exception.message},
                "addició",
                "abonament one-shot addicional",
            ).run()
            raise exception
        except HTTPError as error:
            UpdateTicketWithError(
                self.ticket_id,
                error.__dict__,
                "addició",
                "abonament one-shot addicional",
            ).run()
            raise error

        if new_trv_bond:
            original_tariff = self._get_original_tariff(asset.phone)

        UpdateTicketWithProvisioningInfo(
            self.ticket_id,
            one_shot_bond_msg=ONE_SHOT_BOND_ADDED_MSG.format(
                asset.id, product_code, datetime.today().strftime("%d-%m-%Y %H:%M:%S")
            ),
            original_trv_tariff=original_tariff if new_trv_bond else ""
        ).run()

        logger.info(
            "One shot additional bond accepted. "
            + "Asset id: {}, productAbonamentDadesAddicionals: {}".format(
                asset.id, product_code
            )
        )

    def _build_change_tariff_params(self, asset, tariff, execute_date):
        """
        According to the new tariff and asset information, manage and build the params
        to send to the '/change-asset' MM endpoint
        """
        new_line_request_entry = NewLineRequest.objects.create()

        change_tariff_params = {
            "asset_id": asset.id,
            "transaction_id": new_line_request_entry.get_transaction_id(),
            "product_id": tariff.mm_tariff,
            "execute_date": execute_date,
            "additional_bonds": tariff.additional_bonds,
            "promotions": tariff.promotions
        }

        # We cannot send a product_id to MM if the target asset already has that product.
        if asset.productId == tariff.mm_tariff:

            new_bonds = change_tariff_params.get("additional_bonds")
            promotions = change_tariff_params.get("promotions")
            bonds_to_remove = [bond.get("AssetId") for bond in asset.additionalBonds
                               if bond.get("id") in
                               Tariffs.get_all_recurrent_additional_bonds()]
            promotions_to_remove = [bond.get("AssetId") for bond in asset.promotions]

            if new_bonds or promotions or bonds_to_remove or promotions_to_remove:
                # If the new tariff has new additional bonds, remove the old and and
                # keep the new ones.
                # If the new tariff does not have bonds but the old has, remove them

                change_tariff_params.update({
                    "product_id": "",
                    "bonds_to_remove": bonds_to_remove,
                    "promotions_to_remove": promotions_to_remove,
                })
            else:
                # If it doesn't, the MM composition of the new tariff is exactly the same as the
                # old one, so we raise and exception without calling MM.
                raise NullTariffChangeException(self.ticket_id)

        return change_tariff_params

    def _get_original_tariff(self, phone_number):
        """
        Connect to ODOO (SomConnexió ERP) database to apply a SELECT query
        The query retrieves the corresponding current tariff from a phone number's mobile contract
        In ODOO's database we cannot find more than one contract with the same phone number, so
        this query cannot retrieve more than one line.

        Returns a product code (str)
        """
        query = "SELECT contract_product_code FROM mobile_contract_otrs_view WHERE contract_phone_number='{}'".format(phone_number)  # noqa
        cursor = connections['odoo'].cursor()
        cursor.execute(query)
        query_result = cursor.fetchall()

        # Extract string from the query result (tuple in list).
        # Ex: [('SE_SC_REC_MOBILE_T_150_1024',)]
        return query_result[0][0]
