from api.models import Tariffs, InternationalTariffs


class Tariff:
    """
    Recieves the product_code requested form the SC forms and maps this information to
    a MM tarif, combining basic tariffs and additional bonds if necessary.
    Returns a tariff object with all this information
    """

    def __init__(self, product_code, international_minutes=""):
        self.db_tariff = Tariffs.search_by_product_code(product_code)
        self.mm_tariff = self.db_tariff.mm_tariff
        self.additional_bonds = []
        self.promotions = []

        # Add additional bonds/promotions if required:
        self.additional_bonds.extend([
            self.format_bond_or_promotion(bond) for bond in
            [self.db_tariff.additional_bond, self.db_tariff.additional_bond2] if bond
        ])
        if international_minutes:
            international_bond = InternationalTariffs.search_MM_tariff(
                international_minutes).mm_tariff
            self.additional_bonds.append(self.format_bond_or_promotion(international_bond))

        if self.db_tariff.promotion:
            self.promotions.append(self.format_bond_or_promotion(self.db_tariff.promotion))

    def format_bond_or_promotion(self, code):
        return {"id": "{}".format(code), "qty": 1}
