from api.OTRS_catalan_messages import (
    PROVINCE_NOT_FOUND_MSG,
    TARIF_NOT_FOUND_MSG,
    INTERNATIONAL_TARIF_NOT_FOUND_MSG,
    INVALID_PC_MSG,
    PHONE_NUMBER_WITHOUT_PORTABILITY_MSG,
    ONE_SHOT_ADDITIONAL_BOND_NOT_FOUND_MSG,
    PORTABILITY_WITHOUT_PHONE_NUMBER_MSG,
    NULL_CHANGE_TARIFF_REQUEST
)


class ExceptionToSendToOTRS(Exception):
    """Base exception class with the logic to be sent to OTRS"""

    message = "Abstract message to notify to OTRS"
    action = ""
    object_type = ""
    ticket_id = ""

    def __init__(self, ticket_id):
        self.ticket_id = ticket_id
        super().__init__(self.message)


class ProvinceNotFoundException(Exception):
    """Exception raised when trying to retreive a Province DB object by an
    innexistent OTRS code."""

    message = PROVINCE_NOT_FOUND_MSG

    def __init__(self, code):
        self.message = self.message.format(code)
        super().__init__(self.message)


class TariffNotFoundException(Exception):
    """Exception raised when trying to get a Tariff object with an
    innexistent product code"""

    message = TARIF_NOT_FOUND_MSG

    def __init__(self, code):
        self.message = self.message.format(code)
        super().__init__(self.message)


class OneShotAdditionalBondNotFoundException(Exception):
    """Exception raised when trying to get a one shot additional bond
    with an innexistent product code"""

    message = ONE_SHOT_ADDITIONAL_BOND_NOT_FOUND_MSG

    def __init__(self, code):
        self.message = self.message.format(code)
        super().__init__(self.message)


class InternationalTariffNotFoundException(Exception):
    """Exception raised when trying to retreive an International Tariff object with
    an innexistent quote of minutes"""

    message = INTERNATIONAL_TARIF_NOT_FOUND_MSG

    def __init__(self, minutes):
        self.message = self.message.format(minutes)
        super().__init__(self.message)


class PostalCodeFormatException(Exception):
    """Exception raised when a postal code is not a 5 digit code."""

    message = INVALID_PC_MSG

    def __init__(self, postal_code):
        self.message = self.message.format(postal_code)
        super().__init__(self.message)


class NewNumberRegistrationWithPhoneNumberException(ExceptionToSendToOTRS):
    """Exception raised when we recieve a phone number in a new numbering
    request petition."""

    message = PHONE_NUMBER_WITHOUT_PORTABILITY_MSG
    action = "creació"
    object_type = "OrderItem"

    def __init__(self, ticket_id, phone_number):
        self.message = self.message.format(phone_number)
        super().__init__(ticket_id)


class PortabilityWithoutPhoneNumberException(ExceptionToSendToOTRS):
    """Exception raised when we recieve a phone number in a new numbering
    request petition."""

    message = PORTABILITY_WITHOUT_PHONE_NUMBER_MSG
    action = "creació"
    object_type = "OrderItem"


class NullTariffChangeException(ExceptionToSendToOTRS):
    """Exception raised when we recieve a change tariff request between
    two tariffs that have the exact MM product composition"""

    message = NULL_CHANGE_TARIFF_REQUEST
    action = "canvi de tarifa"
    object_type = "línia mòbil"
