# coding: utf-8
import unittest

from otrs.models.error_article import ErrorArticle


class ErrorArticleTestCase(unittest.TestCase):
    def test_article_instance(self):

        fake_error = {
            "statusCode": "400",
            "message": "El documentType no es un valor válido: 2",
            "fields": "documentType",
        }

        article = ErrorArticle(fake_error, "creació", "Account")

        self.assertEqual(
            article.subject, "Error desde Más Móvil en el/la creació d'un/a Account"
        )
        self.assertEqual(
            article.body,
            "fields: documentType\nmessage: El documentType no es un"
            + " valor válido: 2\nstatusCode: 400\n",
        )
