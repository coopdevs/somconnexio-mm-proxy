from django.test import TestCase

from api.account_mapping import AccountMapping
from api.exceptions import PostalCodeFormatException
from api.somconnexio_contact import SomConnexioContact


class AccountMappingTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def setUp(self):
        """
        Set up variables that will be used for several tests
        """

        # Empty keys from the XSLT mapped body from OTRS calls arrive as `{}`
        self.ticket_data = {
            "road_type": "carrer",
            "road_number": "2",
            "door": {},
            "flat": {},
            "building_portal": {},
            "stair": {},
        }
        self.customer_data = {
            "UserFirstname": "Guita",
            "UserLastname": "Guitona",
            "UserSubdivisionCode": "ES-GI",
            "UserPartyType": "person",
            "UserIdCode": "ES12345F",
            "UserZip": "17820",
            "UserCity": "Banyoles",
            "UserCountryCode": "ES",
        }

        self.organization_data = {
            "UserLastname": "LaPERA SA",
            "UserPartyType": "organization",
            "UserIdCode": "ESF123456",
            "UserZip": "08015",
            "UserSubdivisionCode": "ES-GI",
        }

        self.mapped_account = AccountMapping(self.ticket_data, self.customer_data)

    def test_mapping_fields_account(self):
        self.assertEqual(self.mapped_account.phone, SomConnexioContact.phone)
        self.assertEqual(self.mapped_account.email, SomConnexioContact.email)
        self.assertEqual(self.mapped_account.name, "Guita")
        self.assertEqual(self.mapped_account.surname, "Guitona")
        self.assertEqual(self.mapped_account.documentNumber, "12345F")
        self.assertEqual(self.mapped_account.documentType, "2")
        self.assertEqual(self.mapped_account.nationality, "ES")
        self.assertEqual(self.mapped_account.postalCode, "17820")
        self.assertEqual(self.mapped_account.town, "Banyoles")
        self.assertEqual(self.mapped_account.roadName, None)
        self.assertEqual(self.mapped_account.stair, None)

    def test_mapping_fields_organization_account(self):
        mapped_account = AccountMapping(self.ticket_data, self.organization_data)

        self.assertEqual(mapped_account.name, "LaPERA SA")
        self.assertEqual(mapped_account.surname, "LaPERA SA")
        self.assertEqual(mapped_account.corporateName, "LaPERA SA")
        self.assertEqual(mapped_account.documentType, "1")
        self.assertEqual(mapped_account.documentNumber, "F123456")
        self.assertEqual(mapped_account.roadName, None)
        self.assertEqual(mapped_account.stair, None)

    def test_mapping_fields_NIE_documentType(self):
        self.customer_data.update({"UserIdCode": "ESY8918324L"})

        mapped_account = AccountMapping(self.ticket_data, self.customer_data)

        self.assertEqual(mapped_account.documentNumber, "Y8918324L")
        self.assertEqual(mapped_account.documentType, "3")

    def test_db_mapping(self):
        self.assertEqual(self.mapped_account.province, "17")
        self.assertEqual(self.mapped_account.region, "9")
        self.assertEqual(self.mapped_account.roadType, "Carrer")

    def test_postal_code_ok(self):
        self.assertEqual(self.mapped_account.check_postal_code("17820"), "17820")
        self.assertEqual(self.mapped_account.check_postal_code("8015"), "08015")
        self.assertEqual(self.mapped_account.check_postal_code(" 8015"), "08015")
        self.assertEqual(self.mapped_account.check_postal_code("8015 "), "08015")

    def test_postal_code_ko(self):
        customer_data_bad_postal_code = {
            "UserFirstname": "Guita",
            "UserSubdivisionCode": "ES-GI",
            "UserPartyType": "person",
            "UserIdCode": "ES12345F",
            "UserZip": "S8015",
            "UserCountryCode": "ES",
        }
        self.assertRaises(
            PostalCodeFormatException,
            AccountMapping,
            self.ticket_data,
            customer_data_bad_postal_code,
        )

        self.assertRaises(
            PostalCodeFormatException, self.mapped_account.check_postal_code, "1111s"
        )

        self.assertRaises(
            PostalCodeFormatException, self.mapped_account.check_postal_code, "111111"
        )

        self.assertRaises(
            PostalCodeFormatException, self.mapped_account.check_postal_code, "7015"
        )

        self.assertRaises(
            PostalCodeFormatException, self.mapped_account.check_postal_code, "88"
        )
