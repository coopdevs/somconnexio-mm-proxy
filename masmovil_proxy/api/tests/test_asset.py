from mock import patch
from datetime import datetime
from django.test import TestCase

from pymasmovil.models.asset import Asset as MMAsset
from pymasmovil.errors.http_error import HTTPError
from pymasmovil.errors.exceptions import UnknownMMError

from api.asset import Asset
from api.exceptions import (
    OneShotAdditionalBondNotFoundException,
    TariffNotFoundException,
    NullTariffChangeException
)
from api.models import NewLineRequest, OneShotAdditionalBonds
from api.services.date import Date
from api.tests.fake_mm_error import FakeMMError
from api.OTRS_catalan_messages import (
    ONE_SHOT_ADDITIONAL_BOND_NOT_FOUND_MSG,
    ONE_SHOT_BOND_ADDED_MSG,
    TARIF_NOT_FOUND_MSG,
    TARIFF_CHANGED_MSG,
    NULL_CHANGE_TARIFF_REQUEST
)


class AssetTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def setUp(self):
        self.ticket_id = 777
        self.session = object()
        self.phone = "666666666"
        # Asset with a TRV product
        self.MM_asset = MMAsset(
            id="00002", productId="T00000000000000001",
            phone=self.phone
        )
        self.MM_asset.additionalBonds = [
            {"id": "AB000000000000001",
             "AssetId": "AB000000000000001"},
            {"id": "unknown-id",
             "AssetId": "AB000000000000002"},
        ]

        self.asset = Asset(self.session, self.ticket_id)

        self.additional_bond = {"id": "AB000000000000001", "qty": 1}
        self.expected_date = Date.get_first_day_of_next_month()

        # Errors
        self.sample_error = FakeMMError(
            400, "Debe informar el nombre del cliente", "first_name,last_name"
        )
        self.expected_error = {
            "status_code": 400,
            "fields": "first_name,last_name",
            "message": "Debe informar el nombre del cliente",
        }

    @patch.object(MMAsset, "get_by_phone")
    def test_get_asset_by_phone_ok(self, mock_get_asset_by_phone):

        mock_get_asset_by_phone.return_value = self.MM_asset
        asset = self.asset.get_by_phone(self.phone)

        self.assertEqual(asset, self.MM_asset)
        mock_get_asset_by_phone.assert_called_once_with(self.session, self.phone)

    @patch.object(MMAsset, "get_by_phone")
    @patch("api.asset.UpdateTicketWithError")
    def test_get_asset_by_phone_ko(
        self, MockUpdateTicketWithArticle, mock_get_asset_by_phone
    ):

        mock_get_asset_by_phone.side_effect = HTTPError(self.sample_error)

        self.assertRaises(HTTPError, self.asset.get_by_phone, self.phone)

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, self.expected_error, "obtenció", "Asset"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_change_tariff_same_product_ok(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):
        # Product SE_SC_REC_MOBILE_T_0_700 corresponds to self.MM_asset productId
        self.asset.change_tariff(self.MM_asset, "SE_SC_REC_MOBILE_T_0_700")

        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            product_id="",
            execute_date=self.expected_date,
            additional_bonds=[self.additional_bond],
            bonds_to_remove=["AB000000000000001"],  # only known AssetId from self.MM_asset.additional_bonds  # noqa
            promotions=[],
            promotions_to_remove=[],
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            tariff_change_msg=TARIFF_CHANGED_MSG.format(
                self.MM_asset.id, "SE_SC_REC_MOBILE_T_0_700", self.expected_date
            ),
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_change_tariff_same_product_remove_bonds_and_promotions(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):
        self.MM_asset.promotions = [{
            "id": "P000000000000001",
            "AssetId": "PA000000000000001"
        }]
        # Product SE_SC_REC_MOBILE_T_0_750 corresponds to self.MM_asset productId
        self.asset.change_tariff(self.MM_asset, "SE_SC_REC_MOBILE_T_0_750")

        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            product_id="",
            execute_date=self.expected_date,
            additional_bonds=[],  # No new bonds to add
            bonds_to_remove=["AB000000000000001"],  # only known AssetId from self.MM_asset.additional_bonds  # noqa
            promotions=[],
            promotions_to_remove=["PA000000000000001"],
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            tariff_change_msg=TARIFF_CHANGED_MSG.format(
                self.MM_asset.id, "SE_SC_REC_MOBILE_T_0_750", self.expected_date
            ),
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    def test_change_tariff_null_change(self):
        mm_asset = MMAsset(id="00003", productId="T00000000000000002")

        # Product SE_SC_REC_MOBILE_T_0_1000 corresponds to MM_asset productId,
        # and neither of them have any bonds
        self.assertRaisesRegex(
            NullTariffChangeException,
            NULL_CHANGE_TARIFF_REQUEST,
            self.asset.change_tariff,
            mm_asset,
            "SE_SC_REC_MOBILE_T_0_1000",
        )

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_change_tariff_different_product_ok(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):
        promotion = {"id": "P000000000000001", "qty": 1}

        # Product SE_SC_REC_MOBILE_T_0_900 does not correspond
        # to self.MM_asset productId
        self.asset.change_tariff(self.MM_asset, "SE_SC_REC_MOBILE_T_0_900")

        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            product_id="T00000000000000002",
            execute_date=self.expected_date,
            additional_bonds=[],
            promotions=[promotion],
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            tariff_change_msg=TARIFF_CHANGED_MSG.format(
                self.MM_asset.id, "SE_SC_REC_MOBILE_T_0_900", self.expected_date
            ),
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_change_tariff_change_date_ok(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):
        promotion = {"id": "P000000000000001", "qty": 1}
        change_date = "2022-01-18"

        # Product SE_SC_REC_MOBILE_T_0_900 does not correspond
        # to self.MM_asset productId
        self.asset.change_tariff(self.MM_asset, "SE_SC_REC_MOBILE_T_0_900", change_date)

        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            product_id="T00000000000000002",
            execute_date=change_date,
            additional_bonds=[],
            promotions=[promotion],
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            tariff_change_msg=TARIFF_CHANGED_MSG.format(
                self.MM_asset.id, "SE_SC_REC_MOBILE_T_0_900", change_date
            ),
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithError")
    def test_change_tariff_tariff_not_found(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):

        fake_tarif_code = "SE_SC_REC_MOBILE_T_1_133"

        expected_error_msg = TARIF_NOT_FOUND_MSG.format(fake_tarif_code)
        expected_error_dct = {"missatge": expected_error_msg}

        self.assertRaisesRegex(
            TariffNotFoundException,
            expected_error_msg,
            self.asset.change_tariff,
            self.MM_asset,
            fake_tarif_code,
        )
        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, expected_error_dct, "canvi de tarifa", "línia mòbil"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithError")
    def test_change_tariff_ko(self, MockUpdateTicketWithArticle, mock_update_product):

        mock_update_product.side_effect = HTTPError(self.sample_error)

        self.assertRaises(
            HTTPError,
            self.asset.change_tariff,
            self.MM_asset,
            "SE_SC_REC_MOBILE_T_0_700",
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, self.expected_error, "canvi de tarifa", "línia mòbil"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithError")
    def test_change_tariff_ko_unknown_error(self, MockUpdateTicketWithArticle,
                                            mock_update_product):
        exception_message = "unknown"
        mock_update_product.side_effect = UnknownMMError(exception_message)

        self.assertRaises(
            UnknownMMError,
            self.asset.change_tariff,
            self.MM_asset,
            "SE_SC_REC_MOBILE_T_0_700",
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, {"missatge": exception_message},
            "canvi de tarifa", "línia mòbil"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_add_one_shot_additional_bond_ok(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):
        # Asset with non-TRV product
        self.MM_asset.productId = "T00000000000000002"
        self.additional_bond["id"] = "01t4I0000079CM9QAM"

        self.asset.add_one_shot_additional_bond(
            self.MM_asset, "CH_SC_OSO_500MB_ADDICIONAL"
        )

        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            additional_bonds=[self.additional_bond],
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            one_shot_bond_msg=ONE_SHOT_BOND_ADDED_MSG.format(
                self.MM_asset.id,
                "CH_SC_OSO_500MB_ADDICIONAL",
                datetime.today().strftime("%d-%m-%Y %H:%M:%S"),
            ),
            original_trv_tariff=""
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_add_one_shot_additional_bond_to_TRV_same_bond_ok(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):
        # We must remove the original TRV bond and add it again "clean" to be able
        # to continue consuming. But, as it is the same, we don't need to send to OTRS
        # the original bond, no more changes will be required, and at next month the
        # bond will be automatically reseted having the same original tariff.

        add_bond_product_code = "CH_SC_OSO_700MB_ADDICIONAL"

        # self.MM_asset has the same TRV bond as the additional bond to be added
        self.asset.add_one_shot_additional_bond(
            self.MM_asset, add_bond_product_code
        )
        one_shot_additional_bond = OneShotAdditionalBonds.search_by_product_code(
            add_bond_product_code
        )
        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            additional_bonds=[{"id": one_shot_additional_bond.trv_bond, "qty": 1}],
            bonds_to_remove=[self.MM_asset.additionalBonds[0]["AssetId"]]
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            one_shot_bond_msg=ONE_SHOT_BOND_ADDED_MSG.format(
                self.MM_asset.id,
                add_bond_product_code,
                datetime.today().strftime("%d-%m-%Y %H:%M:%S"),
            ),
            original_trv_tariff=""
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(Asset, "_get_original_tariff")
    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_add_one_shot_additional_bond_to_TRV_without_bonds_ok(
        self, MockUpdateTicketWithArticle, mock_update_product, mock_get_original_tariff
    ):

        expected_product_code = "example_code"
        add_bond_product_code = "CH_SC_OSO_700MB_ADDICIONAL"
        self.MM_asset.additionalBonds = []

        def mock_get_original_tariff_side_effect(phone_number):
            if phone_number == self.MM_asset.phone:
                return expected_product_code

        mock_get_original_tariff.side_effect = mock_get_original_tariff_side_effect

        # self.MM_asset has the same TRV bond as the additional bond to be added
        self.asset.add_one_shot_additional_bond(
            self.MM_asset, add_bond_product_code
        )
        one_shot_additional_bond = OneShotAdditionalBonds.search_by_product_code(
            add_bond_product_code
        )
        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            additional_bonds=[{"id": one_shot_additional_bond.trv_bond, "qty": 1}],
            bonds_to_remove=[]
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            one_shot_bond_msg=ONE_SHOT_BOND_ADDED_MSG.format(
                self.MM_asset.id,
                add_bond_product_code,
                datetime.today().strftime("%d-%m-%Y %H:%M:%S"),
            ),
            original_trv_tariff=expected_product_code
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(Asset, "_get_original_tariff")
    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithProvisioningInfo")
    def test_add_one_shot_additional_bond_to_TRV_different_bond_ok(
        self, MockUpdateTicketWithArticle, mock_update_product, mock_get_original_tariff
    ):
        # We must remove the original TRV bond and add the new one. Also, we need to
        # send the original TRV to OTRS to be able to recover it as the regular
        # tariff before we start the next month (we are changing the original tariff
        # here, and we must take the changes back).

        expected_product_code = "example_code"

        def mock_get_original_tariff_side_effect(phone_number):
            if phone_number == self.MM_asset.phone:
                return expected_product_code

        add_bond_product_code = "CH_SC_OSO_500MB_ADDICIONAL"
        mock_get_original_tariff.side_effect = mock_get_original_tariff_side_effect

        # self.MM_asset has a different TRV bond from the additional bond to be added
        self.asset.add_one_shot_additional_bond(
            self.MM_asset, add_bond_product_code
        )
        one_shot_additional_bond = OneShotAdditionalBonds.search_by_product_code(
            add_bond_product_code
        )
        mock_update_product.assert_called_once_with(
            self.session,
            asset_id=self.MM_asset.id,
            transaction_id=NewLineRequest.objects.all().last().get_transaction_id(),
            additional_bonds=[{"id": one_shot_additional_bond.trv_bond, "qty": 1}],
            bonds_to_remove=[self.MM_asset.additionalBonds[0]["AssetId"]]
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            one_shot_bond_msg=ONE_SHOT_BOND_ADDED_MSG.format(
                self.MM_asset.id,
                add_bond_product_code,
                datetime.today().strftime("%d-%m-%Y %H:%M:%S"),
            ),
            original_trv_tariff=expected_product_code
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithError")
    def test_add_one_shot_additional_bond_ko(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):

        mock_update_product.side_effect = HTTPError(self.sample_error)

        self.assertRaises(
            HTTPError,
            self.asset.add_one_shot_additional_bond,
            self.MM_asset,
            "CH_SC_OSO_500MB_ADDICIONAL",
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            self.expected_error,
            "addició",
            "abonament one-shot addicional",
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMAsset, "update_product")
    @patch("api.asset.UpdateTicketWithError")
    def test_add_one_shot_additional_bond_ko_unknown_error(
        self, MockUpdateTicketWithArticle, mock_update_product
    ):
        exception_message = "unknown"
        mock_update_product.side_effect = UnknownMMError(exception_message)

        self.assertRaises(
            UnknownMMError,
            self.asset.add_one_shot_additional_bond,
            self.MM_asset,
            "CH_SC_OSO_500MB_ADDICIONAL",
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            {"missatge": exception_message},
            "addició",
            "abonament one-shot addicional",
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.asset.UpdateTicketWithError")
    def test_add_one_shot_additional_bond_not_found(self, MockUpdateTicketWithArticle):

        expected_error_msg = ONE_SHOT_ADDITIONAL_BOND_NOT_FOUND_MSG.format(
            "CH_SC_OSO_600MB_ADDICIONAL"
        )
        expected_error_dct = {"missatge": expected_error_msg}

        self.assertRaisesRegex(
            OneShotAdditionalBondNotFoundException,
            expected_error_msg,
            self.asset.add_one_shot_additional_bond,
            self.MM_asset.id,
            "CH_SC_OSO_600MB_ADDICIONAL",
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id,
            expected_error_dct,
            "addició",
            "abonament one-shot addicional",
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()
