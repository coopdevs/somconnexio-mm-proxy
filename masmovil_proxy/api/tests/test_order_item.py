from mock import patch, Mock
from django.test import TestCase

from pymasmovil.models.order_item import OrderItem as MMOrderItem
from pymasmovil.errors.http_error import HTTPError
from pymasmovil.errors.exceptions import (NewLineRequestRequiredParamsError,
                                          OrderItemNotFoundByICC)

from api.order_item import OrderItem
from api.tests.fake_mm_error import FakeMMError
from api.exceptions import (
    TariffNotFoundException,
    InternationalTariffNotFoundException,
)
from api.OTRS_catalan_messages import (
    TARIF_NOT_FOUND_MSG,
    INTERNATIONAL_TARIF_NOT_FOUND_MSG,
)


class FakeNewLineRequest:
    def __init__(self, ICC):
        self.transactionId = 123
        self.lineInfo = [{"iccid": ICC}]


class OrderItemTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def setUp(self):
        self.ticket_id = 777
        self.fake_id = "12345S"
        self.ticket_data = {"mobile_service_type": "portabilitat"}
        self.session = object()
        self.account = Mock(spec=["id", "lineInfo", "get_order_item_by_ICC"])
        self.order_item = OrderItem(self.session, self.ticket_id)
        self.expected_ICC = 282828
        self.fake_new_line_request = FakeNewLineRequest(self.expected_ICC)
        self.expected_order_item = Mock(spec=["id"])
        self.expected_order_item.id = "00001"
        self.call_count = 0

        # Errors
        self.sample_error = FakeMMError(
            400, "Debe informar el nombre del cliente", "first_name,last_name"
        )

        self.expected_error = {
            "status_code": 400,
            "fields": "first_name,last_name",
            "message": "Debe informar el nombre del cliente",
        }

    @patch.object(MMOrderItem, "create")
    @patch("api.order_item.NewLineRequestMapping")
    def test_create_mm_order_item_ok(self, MockNLRMapping, mock_order_item_create):
        """
        Check that the `OrderItem.create` method from PyMasMovil is called
        and that we can get the just created order-item
        """
        MockNLRMapping.return_value = self.fake_new_line_request

        self.account.get_order_item_by_ICC.return_value = self.expected_order_item

        order_item = self.order_item.create(self.account, self.ticket_data)

        self.assertEqual(order_item, self.expected_order_item)

        self.account.get_order_item_by_ICC.assert_called_once_with(
            self.session, self.expected_ICC, True
        )
        mock_order_item_create.assert_called_once_with(
            self.session, self.account, **self.fake_new_line_request.__dict__
        )

    @patch("api.order_item.NewLineRequestMapping")
    @patch("api.order_item.UpdateTicketWithError")
    def test_wrong_tarif_mm_orderItem_mapping(
        self, MockUpdateTicketWithArticle, MockNLRMapping
    ):
        """
        Check that if TariffNotFoundException is raised when mapping, we send an error
        to OTRS
        """
        MockNLRMapping.side_effect = TariffNotFoundException("SE_SC_REC_MOBILE_FAKE")

        expected_error_msg = TARIF_NOT_FOUND_MSG.format("SE_SC_REC_MOBILE_FAKE")
        expected_error_dct = {"missatge": expected_error_msg}

        self.assertRaisesRegex(
            TariffNotFoundException,
            expected_error_msg,
            self.order_item.create,
            self.account,
            self.ticket_data,
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, expected_error_dct, "creació", "OrderItem"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch("api.order_item.NewLineRequestMapping")
    @patch("api.order_item.UpdateTicketWithError")
    def test_wrong_international_tarif_mm_orderItem_mapping(
        self, MockUpdateTicketWithArticle, MockNLRMapping
    ):
        """
        Check that if InternationalTariffNotFoundException is raised when mapping, we send an error
        to OTRS
        """
        MockNLRMapping.side_effect = InternationalTariffNotFoundException("37m")

        expected_error_msg = INTERNATIONAL_TARIF_NOT_FOUND_MSG.format("37m")
        expected_error_dct = {"missatge": expected_error_msg}

        self.assertRaisesRegex(
            InternationalTariffNotFoundException,
            expected_error_msg,
            self.order_item.create,
            self.account,
            self.ticket_data,
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, expected_error_dct, "creació", "OrderItem"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMOrderItem, "create")
    @patch("api.order_item.NewLineRequestMapping")
    @patch("api.order_item.UpdateTicketWithError")
    def test_create_mm_order_item_ko(
        self, MockUpdateTicketWithArticle, MockNLRMapping, mock_order_item_create
    ):
        """
        Check that an error message is send to OTRS with UpdateTicketWithError
        when an order_item creation process fails. Then, the same error is reraised
        """
        mock_order_item_create.side_effect = HTTPError(self.sample_error)

        self.assertRaises(
            HTTPError, self.order_item.create, self.account, self.ticket_data
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, self.expected_error, "creació", "OrderItem"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMOrderItem, "create")
    @patch("api.order_item.NewLineRequestMapping")
    @patch("api.order_item.UpdateTicketWithError")
    def test_incomplete_mm_orderItem_mapping(
        self, MockUpdateTicketWithArticle, MockNLRMapping, mock_order_item_create
    ):
        """
        Check that if NewLineRequestRequiredParamsError is raised when mapping, we send an error
        to OTRS
        """
        mock_order_item_create.side_effect = NewLineRequestRequiredParamsError(True, [])

        self.assertRaises(
            NewLineRequestRequiredParamsError,
            self.order_item.create,
            self.account,
            self.ticket_data,
        )

        MockUpdateTicketWithArticle.assert_called_once()
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()

    @patch.object(MMOrderItem, "create")
    @patch("api.order_item.NewLineRequestMapping")
    @patch("time.sleep")
    def test_get_mm_order_item_by_ICC_after_created_second_call_ok(
        self, _, MockNLRMapping, mock_order_item_create
    ):
        """
        Check that the `OrderItem.create` method from PyMasMovil is called
        and that we recieve the just created order-item after one failed call.
        """

        MockNLRMapping.return_value = self.fake_new_line_request

        def _get_order_item_by_ICC_side_effect(session, ICC, non_KO):
            if session == self.session and ICC == self.expected_ICC and non_KO:
                self.call_count += 1
                while self.call_count < 2:
                    raise HTTPError(self.sample_error)
                else:
                    return self.expected_order_item

        self.account.get_order_item_by_ICC.side_effect = (
            _get_order_item_by_ICC_side_effect
        )

        order_item = self.order_item.create(self.account, self.ticket_data)

        self.assertEqual(order_item, self.expected_order_item)

        mock_order_item_create.assert_called_once_with(
            self.session, self.account, **self.fake_new_line_request.__dict__
        )

    @patch.object(MMOrderItem, "create")
    @patch("api.order_item.NewLineRequestMapping")
    @patch("time.sleep")
    def test_get_mm_order_item_by_ICC_after_created_third_call_ok(
        self, _, MockNLRMapping, mock_order_item_create
    ):
        """
        Check that the `OrderItem.create` method from PyMasMovil is called
        and that we recieve the just created order-item by matching ICC
        after two failed calls.
        """
        MockNLRMapping.return_value = self.fake_new_line_request

        def _get_order_item_by_ICC_side_effect(session, ICC, non_KO):
            if session == self.session and ICC == self.expected_ICC and non_KO:
                self.call_count += 1
                if self.call_count < 2:
                    raise HTTPError(self.sample_error)
                elif self.call_count == 2:
                    raise OrderItemNotFoundByICC(ICC, self.account.id)
                else:
                    return self.expected_order_item

        self.account.get_order_item_by_ICC.side_effect = (
            _get_order_item_by_ICC_side_effect
        )

        order_item = self.order_item.create(self.account, self.ticket_data)

        self.assertEqual(order_item, self.expected_order_item)

        mock_order_item_create.assert_called_once_with(
            self.session, self.account, **self.fake_new_line_request.__dict__
        )

    @patch.object(MMOrderItem, "create")
    @patch("api.order_item.NewLineRequestMapping")
    @patch("api.order_item.UpdateTicketWithError")
    @patch("time.sleep")
    def test_get_mm_order_item_by_ICC_after_created_three_calls_ko(
        self, _, MockUpdateTicketWithArticle, MockNLRMapping, mock_order_item_create
    ):
        """
        Check that an error message is send to OTRS with UpdateTicketWithError
        when an order_item creation process succeds but we cannot find an order-item
        with the given ICC after trying three calls.
        """
        self.ticket_data.update({"mobile_service_type": "altaNova"})
        self.account.id = "0001"
        MockNLRMapping.return_value = self.fake_new_line_request
        expected_error_msg = \
            "No order item with ICC: {} can be found in the account with id: {}".format(
                self.expected_ICC, self.account.id
            )
        expected_error_dct = {"missatge": expected_error_msg}

        def _get_order_item_by_ICC_side_effect(session, ICC, non_KO):
            if session == self.session and ICC == self.expected_ICC and non_KO:
                self.call_count += 1
                while self.call_count <= 3:
                    raise OrderItemNotFoundByICC(ICC, self.account.id)
                else:
                    return self.expected_order_item

        self.account.get_order_item_by_ICC.side_effect = (
            _get_order_item_by_ICC_side_effect
        )

        self.assertRaises(
            OrderItemNotFoundByICC, self.order_item.create, self.account,
            self.ticket_data
        )

        MockUpdateTicketWithArticle.assert_called_once_with(
            self.ticket_id, expected_error_dct, "obtenció", "OrderItem"
        )
        MockUpdateTicketWithArticle.return_value.run.assert_called_once_with()
