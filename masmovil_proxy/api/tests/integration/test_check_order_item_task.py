import os
from mock import patch, Mock
from django.test import TestCase
from pyotrs.lib import DynamicField

from pymasmovil.models.session import Session
from pymasmovil.models.asset import Asset as MMAsset
from pymasmovil.models.order_item import OrderItem as MMOrderItem
from otrs_somconnexio.client import OTRSClient

from api.tasks import check_order_item


class CheckOrderItemTaskIntegrationTestCase(TestCase):
    @patch.object(Session, "create")
    @patch.object(MMOrderItem, "get")
    @patch.object(MMAsset, "get_by_phone")
    @patch.object(OTRSClient, "update_ticket")
    @patch("otrs_somconnexio.otrs_models.abstract_article.Article")
    @patch("otrs_somconnexio.services.update_ticket_with_provider_info.DynamicField")
    @patch("otrs_somconnexio.client.Client")
    @patch.dict(
        os.environ,
        {
            "OTRS_USER": "user",
            "OTRS_PASSW": "passw",
            "OTRS_URL": "https://otrs-url.coop/",
        },
    )
    def test_check_order_item_completed(
        self,
        MockOTRSClient,
        MockDF,
        MockPyOTRSArticle,
        mock_update_ticket,
        mock_asset_get_by_phone,
        mock_order_item_get,
        mock_session_create,
    ):
        """
        Check that the order item status is completed, the corresponding asset is
        searched and sent to OTRS
        """

        activate_date = "2021-06-17 00:00:00"
        request_body = {
            "ticket_id": 111,
            "mm_order_item_id": "0002",
            "activate_date": activate_date
        }
        mock_session = Mock(set_spec=["id"])
        mock_session.id = "0001"
        phone = "666666666"

        mock_order_item = Mock(set_spec=["id", "phone", "status"])
        mock_order_item.id = "0002"
        mock_order_item.status = "Completada"
        mock_order_item.phone = phone

        asset = MMAsset(id="0003", phone=phone)
        pyOTRS_article = object()
        article_params = {
            "Subject": "Línia de mòbil donada d'alta a MasMóvil",
            "Body": u"id: 0003\nphone: 666666666\n",
            "ContentType": "text/plain; charset=utf8",
            "IsVisibleForCustomer": "1"
        }

        df_activated_phone = DynamicField(name="contracteMobilActivat", value=1)
        df_phone_number = DynamicField(name="liniaMobil", value=phone)
        df_checkbox = DynamicField(name="enviarCheckOrderItem", value=0)
        df_billing_date = DynamicField(name="dataIniciFacturacio", value=activate_date)

        def mock_df_side_effect(name, value):
            if name == "contracteMobilActivat" and value == 1:
                return df_activated_phone
            elif name == "liniaMobil" and value == phone:
                return df_phone_number
            elif name == "enviarCheckOrderItem" and value == 0:
                return df_checkbox
            elif name == "dataIniciFacturacio" and value == activate_date:
                return df_billing_date

        mock_session_create.return_value = mock_session
        mock_order_item_get.return_value = mock_order_item
        mock_asset_get_by_phone.return_value = asset
        MockDF.side_effect = mock_df_side_effect
        MockPyOTRSArticle.return_value = pyOTRS_article

        # Task call
        check_order_item(request_body)

        # Test asserts
        mock_order_item_get.assert_called_once_with(mock_session, mock_order_item.id)
        mock_asset_get_by_phone.assert_called_once_with(mock_session, phone)
        MockPyOTRSArticle.assert_called_once_with(article_params)
        mock_update_ticket.assert_called_once_with(
            111,
            pyOTRS_article,
            dynamic_fields=[df_activated_phone, df_checkbox,
                            df_phone_number, df_billing_date],
        )
