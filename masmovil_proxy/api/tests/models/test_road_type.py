from django.test import TestCase

from api.models import RoadType


class RoadTypeTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def test_search_by_OTRS_code(self):
        self.assertEqual(RoadType.search_by_OTRS_code("cami").mm_code, "Camí")

    def test_search_by_OTRS_code_default(self):
        self.assertEqual(RoadType.search_by_OTRS_code("carreret").mm_code, "Carrer")
