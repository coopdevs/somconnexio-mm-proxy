from django.test import TestCase
from api.models import Tariffs
from api.exceptions import TariffNotFoundException


class TariffsTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def test_search_by_product_code(self):
        self.assertIsInstance(
            Tariffs.search_by_product_code("SE_SC_REC_MOBILE_T_0_700"), Tariffs)

    def test_search_by_product_code_KO(self):
        self.assertRaises(
            TariffNotFoundException,
            Tariffs.search_by_product_code,
            "fake-code"
        )

    def test_get_all_recurrent_additional_bonds(self):
        self.assertEqual(Tariffs.get_all_recurrent_additional_bonds().sort(),
                         ["01t4I0000079CLoQAM", "AB000000000000001"].sort())
