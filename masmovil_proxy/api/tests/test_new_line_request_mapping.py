from django.test import TestCase

from api.models import NewLineRequest
from api.new_line_request_mapping import NewLineRequestMapping
from api.exceptions import (NewNumberRegistrationWithPhoneNumberException,
                            PortabilityWithoutPhoneNumberException)
from api.OTRS_catalan_messages import (PHONE_NUMBER_WITHOUT_PORTABILITY_MSG,
                                       PORTABILITY_WITHOUT_PHONE_NUMBER_MSG)


class FakeAccount:
    def __init__(self, documentNumber, name="", surname="", corporateName=""):
        self.documentNumber = documentNumber
        self.name = name
        self.surname = surname
        self.corporateName = corporateName


class NewLineRequestMappingTestCase(TestCase):
    fixtures = ["api/tests/fixtures/test_db"]

    def setUp(self):
        """
        Set up variables that will be used for several tests
        """

        # Empty keys from the XSLT mapped body from OTRS calls arrive as `{}`
        self.ticket_data = {
            "mobile_service_type": "portabilitat",
            "ICC_SC": "0000001",
            "product_code": "SE_SC_REC_MOBILE_T_0_800",
            "phone_number": "666666666",
            "donor_ICC": "0000002",
            "donor_operator": "Movistar",
            "name": {},
            "surname": {},
            "international_minutes": {},
        }

        self.particular_account = FakeAccount(
            documentNumber="22222222T", name="NomTest", surname="CognomTest"
        )
        self.corporate_account = FakeAccount(
            documentNumber="C48876221", corporateName="SomEnergia"
        )

    def test_portability_corporation(self):
        new_line_request = NewLineRequestMapping(
            self.ticket_data, self.corporate_account
        )

        self.assertEqual(
            new_line_request.transactionId,
            NewLineRequest.objects.all().last().get_transaction_id(),
        )
        self.assertEqual(new_line_request.lineInfo[0]["corporateName"], "SomEnergia")
        self.assertEqual(new_line_request.lineInfo[0]["docid"], "C48876221")
        self.assertEqual(new_line_request.lineInfo[0]["documentType"], "CIF")
        self.assertEqual(
            new_line_request.lineInfo[0]["productInfo"]["id"], "T00000000000000002"
        )
        self.assertTrue("promotions" not in new_line_request.lineInfo[0]["productInfo"])
        self.assertFalse(new_line_request.lineInfo[0]["productInfo"]["additionalBonds"])

    def test_portability_old_particular_with_corporation_account(self):
        self.ticket_data.update(
            {
                "name": "Ferran",
                "surname": "Vila",
                "docid": "ES12345F",
            }
        )
        new_line_request = NewLineRequestMapping(
            self.ticket_data, self.corporate_account
        )

        self.assertEqual(
            new_line_request.transactionId,
            NewLineRequest.objects.all().last().get_transaction_id(),
        )
        self.assertEqual(new_line_request.lineInfo[0]["name"], "Ferran")
        self.assertEqual(new_line_request.lineInfo[0]["surname"], "Vila")
        self.assertEqual(new_line_request.lineInfo[0]["docid"], "12345F")
        self.assertEqual(new_line_request.lineInfo[0]["documentType"], "NIF")

    def test_product_with_promotions(self):
        self.ticket_data.update({"product_code": "SE_SC_REC_MOBILE_T_0_900"})
        new_line_request = NewLineRequestMapping(self.ticket_data, self.particular_account)

        self.assertEqual(new_line_request.lineInfo[0]["productInfo"]["promotions"],
                         [{'id': 'P000000000000001', 'qty': 1}])

    def test_portability_new_particular(self):
        self.ticket_data.update(
            {
                "name": "Ferran",
                "surname": "Vila",
                "docid": "ES12345F",
            }
        )

        new_line_request = NewLineRequestMapping(
            self.ticket_data, self.particular_account
        )

        self.assertEqual(
            new_line_request.transactionId,
            NewLineRequest.objects.all().last().get_transaction_id(),
        )
        self.assertEqual(new_line_request.lineInfo[0]["name"], "Ferran")
        self.assertEqual(new_line_request.lineInfo[0]["surname"], "Vila")
        self.assertEqual(new_line_request.lineInfo[0]["docid"], "12345F")
        self.assertEqual(new_line_request.lineInfo[0]["documentType"], "NIF")
        self.assertEqual(new_line_request.lineInfo[0]["phoneNumber"], "666666666")
        self.assertEqual(new_line_request.lineInfo[0]["iccid_donante"], "0000002")
        self.assertEqual(new_line_request.lineInfo[0]["donorOperator"], "1")
        self.assertEqual(
            new_line_request.lineInfo[0]["productInfo"]["id"], "T00000000000000002"
        )
        self.assertFalse(new_line_request.lineInfo[0]["productInfo"]["additionalBonds"])

    def test_portability_same_particular(self):
        new_line_request = NewLineRequestMapping(
            self.ticket_data, self.particular_account
        )

        self.assertEqual(
            new_line_request.transactionId,
            NewLineRequest.objects.all().last().get_transaction_id(),
        )
        self.assertEqual(new_line_request.lineInfo[0]["name"], "NomTest")
        self.assertEqual(new_line_request.lineInfo[0]["surname"], "CognomTest")
        self.assertEqual(new_line_request.lineInfo[0]["docid"], "22222222T")
        self.assertEqual(new_line_request.lineInfo[0]["documentType"], "NIF")
        self.assertEqual(new_line_request.lineInfo[0]["phoneNumber"], "666666666")
        self.assertEqual(new_line_request.lineInfo[0]["iccid_donante"], "0000002")
        self.assertEqual(new_line_request.lineInfo[0]["donorOperator"], "1")

    def test_new_registration_with_phone_numer(self):
        self.ticket_data["mobile_service_type"] = "altaNova"

        expected_error_msg = PHONE_NUMBER_WITHOUT_PORTABILITY_MSG.format(
            self.ticket_data["phone_number"]
        )

        self.assertRaisesRegex(
            NewNumberRegistrationWithPhoneNumberException,
            expected_error_msg,
            NewLineRequestMapping,
            self.ticket_data,
            self.particular_account,
        )

    def test_portability_without_phone_number(self):
        self.ticket_data["phone_number"] = ""

        self.assertRaisesRegex(
            PortabilityWithoutPhoneNumberException,
            PORTABILITY_WITHOUT_PHONE_NUMBER_MSG,
            NewLineRequestMapping,
            self.ticket_data,
            self.particular_account,
        )

    def test_portability_new_NIE_particular(self):
        self.ticket_data.update({"docid": "ESX6159368Z"})

        new_line_request = NewLineRequestMapping(
            self.ticket_data, self.particular_account
        )

        self.assertEqual(
            new_line_request.transactionId,
            NewLineRequest.objects.all().last().get_transaction_id(),
        )
        self.assertEqual(new_line_request.lineInfo[0]["docid"], "X6159368Z")
        self.assertEqual(new_line_request.lineInfo[0]["documentType"], "NIE")

    def test_portability_new_CIF_particular(self):
        self.ticket_data.update({
            "docid": "ESW6859476A",
            "name": "Limnos"
        })

        new_line_request = NewLineRequestMapping(
            self.ticket_data, self.particular_account
        )

        self.assertEqual(
            new_line_request.transactionId,
            NewLineRequest.objects.all().last().get_transaction_id(),
        )
        self.assertEqual(new_line_request.lineInfo[0]["docid"], "W6859476A")
        self.assertEqual(new_line_request.lineInfo[0]["documentType"], "CIF")
        self.assertEqual(new_line_request.lineInfo[0]["corporateName"], "Limnos")
