# coding: utf-8
from django.test import TestCase
from api.services.vat import VAT


class VATTestCase(TestCase):

    def test_VAT_formating_in_init(self):
        DNI = "4615081Q"
        docid = VAT(DNI)
        self.assertEqual(docid.vat, "ES" + DNI)

    def test_spanish_VAT_to_NIF(self):
        vat = "ES04615081Q"
        docid = VAT(vat)

        self.assertFalse(docid.contains_NIE())
        self.assertEqual(docid.remove_VAT_format(), vat[2:])

    def test_NIE(self):
        vat = "ESX6159368Z"
        docid = VAT(vat)
        self.assertTrue(docid.contains_NIE())

    def test_CIF(self):
        vat = "ESW6859476A"
        docid = VAT(vat)
        self.assertTrue(docid.contains_CIF())
