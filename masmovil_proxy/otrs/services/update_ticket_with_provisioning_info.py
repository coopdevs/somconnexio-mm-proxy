from otrs.models.asset_article import AssetArticle
from otrs.models.order_item_article import OrderItemArticle
from otrs.models.one_shot_bond_article import OneShotBondArticle
from otrs.models.tariff_change_article import TariffChangeArticle
from otrs_somconnexio.services.update_ticket_with_provider_info import (
    UpdateTicketWithProviderInfo,
)


class UpdateTicketWithProvisioningInfo(UpdateTicketWithProviderInfo):
    """
    Update the ticket process with an article to inform that the order item has
    been completed in the MM platform, becoming an activated contract.

    Params:
    ticket_id (str) -> id from the involved OTRS ticket to which an article will be send
    + different optional variables depending on what provisioning info we are sending.
    """

    def __init__(
        self,
        ticket_id,
        order_item_created_msg="",
        mm_order_item_id="",
        phone_number="",
        tariff_change_msg="",
        one_shot_bond_msg="",
        original_trv_tariff="",
        asset=None,
        billing_date=None,
    ):
        self.ticket_id = ticket_id
        self.df_dct = {}

        if mm_order_item_id:
            self.article = OrderItemArticle(order_item_created_msg)
            self.df_dct = {
                "introPlataforma": 1,
                "enviarCreateOrderItem": 0,
                "MMOrderItemId": mm_order_item_id,
                "liniaMobil": phone_number,
            }
        elif one_shot_bond_msg:
            self.article = OneShotBondArticle(one_shot_bond_msg)
            self.df_dct = {
                "dadesOK": 1,
                "RecuperarTarifaTRV": 0,
            }
            if original_trv_tariff:
                self.df_dct.update({
                    "tarifaTRVOriginal": original_trv_tariff,
                    "RecuperarTarifaTRV": 1,
                })

        elif asset:
            self.article = AssetArticle(asset.__dict__)
            self.df_dct = {
                "contracteMobilActivat": 1,
                "enviarCheckOrderItem": 0,
                "liniaMobil": asset.phone,
                "dataIniciFacturacio": billing_date,
            }
        elif tariff_change_msg:
            self.article = TariffChangeArticle(tariff_change_msg)
            self.df_dct = {
                "canviOK": 1,
            }
