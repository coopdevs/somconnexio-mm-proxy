# coding: utf-8
from otrs_somconnexio.otrs_models.abstract_article import AbstractArticle


class OrderItemArticle(AbstractArticle):
    """
    Creates an article informing that the order-item request has been succesfully completed

    Params:
    body (str) -> custom message
    """

    def __init__(self, body):
        self.subject = "Petició d'alta a MM entrada correctament"
        self.body = body
