# coding: utf-8
from otrs_somconnexio.otrs_models.abstract_article import AbstractArticle


class TariffChangeArticle(AbstractArticle):
    """
    Creates an article informing that the tariff change request has been succesfully completed

    Params:
    body (str) -> custom message
    """

    def __init__(self, body):
        self.subject = "Canvi de tarifa correctament realitzat"
        self.body = body
