# Más Móvil API Proxy

Proxy to connect to Más Móvil's API.

## Usage

This is still in the early stages of development so please, read [#development] below.

To see the API in action fire off a dev server running:

```
$ python masmovil_proxy/manage.py runserver
```

Remember to expose the required env vars before booting the server.

Then, you can send a request like `http://127.0.0.1:8000/api/accounts/0017E000017pEo3QAE/` from your browser or curl.

## Development

### Python version

We use [Pyenv](https://github.com/pyenv/pyenv) to fix the Python version and the virtualenv to develop the package.

You need to:

* Install and configure [`pyenv`](https://github.com/pyenv/pyenv)
* Install and configure [`pyenv-virtualenv`](https://github.com/pyenv/pyenv-virtualenv)
* Install the required Python version:

```
$ pyenv install 3.8.2
```

* Create the virtualenv:

```
$ pyenv virtualenv 3.8.2 somconnexio-masmovil-proxy
```

### Setup a development server

We have a provisioning project [`somconnexio-mm-proxy-provisioning`](https://gitlab.com/coopdevs/somconnexio-mm-proxy-provisioning) to create an isolated environment and to install all the dependencies and services running in it.

Please, follow the steps in the project [README](https://gitlab.com/coopdevs/somconnexio-mm-proxy-provisioning/-/blob/master/README.md). Here you'll find instructions on how to start Django and Celery, among other things.

## Formatting

We use [Black](https://github.com/psf/black) as formatter.
Before making a commit, run the `black` command:

```commandline
$ black .
All done! ✨ 🍰 ✨
77 files left unchanged.
```

### Testing

Django tests need to be run from the mentioned environment. We need first to log into with our nominal user and then move to the `mm-proxy` user:

```
$ ssh <user>@somconnexio-mm-proxy.local
$ sudo su - mm-proxy
```

And move to the application directory:
```
$ cd /var/www/somconnexio-mm-proxy/masmovil_proxy
```

Then, we must activate the virtual environment:
```
$ pyenv activate somconnexio-mm-proxy
```

Then, we can execute the tests:
```
$ python manage.py test -v 2
```

If the database gets destroyed and built again, before testing you will need to recreate the DB schema and fill it with some initial data. To do so, run:

```
$ python manage.py migrate
$ python manage.py importcsv --mappings='' --model='api.tariffs' --delimiter=',' api/fixtures/tariffs.csv
$ python manage.py importcsv --mappings='' --model='api.internationalTariffs' --delimiter=',' api/fixtures/internationalTariffs.csv
$ python manage.py importcsv --mappings='' --model='api.region' --delimiter=',' api/fixtures/region.csv
$ python manage.py importcsv --mappings='' --model='api.province' --delimiter=',' api/fixtures/province.csv
$ python manage.py importcsv --mappings='' --model='api.donorOperator' --delimiter=',' api/fixtures/donorOperator.csv
$ python manage.py importcsv --mappings='' --model='api.roadType' --delimiter=',' api/fixtures/roadType.csv
$ python manage.py importcsv --mappings='' --model='api.oneShotAdditionalBonds' --delimiter=',' api/fixtures/oneShotAdditionalBonds.csv
$ python manage.py importcsv --mappings='' --model='api.trvproducts' --delimiter=',' api/fixtures/TRVProducts.csv
```

Because of the incorporation of [django-csvimport](https://github.com/edcrewe/django-csvimport) in this application, we runned into an error when testing the application in the gitlab contininous integration.
The problem has been solved with a workaround solution, which is explained in the [issue](https://github.com/edcrewe/django-csvimport/issues/105) sended to the django-csvimport github project. We also put another [issue](https://gitlab.com/coopdevs/somconnexio-mm-proxy/-/issues/3) in our project to remind that our solution is just temporal and that we need to keep an eye on this.

## Database

### Create / modify tables

The process to start with our DB schema is well explained in Django's documentation, creating [models](https://docs.djangoproject.com/en/3.0/topics/db/models/) that map into DB tables.

For the moment, our models are found in the `api` app (`masmovil_proxy/api/models.py`). From there, we can:
- **create a new table** defining a new class in file.
- **add or delete columns** for a given table, just adding or deleting attributes to its corresponding class.
Changes will need to be [migrated](https://docs.djangoproject.com/en/3.0/topics/migrations/) afterwards to appear in DB.

### Configuration

To give database access to our Django app we need to [configure](https://docs.djangoproject.com/en/3.0/ref/databases/#databases) the PostgreSQL database file in our `settings.py` file.

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('MM_PROXY_DB_NAME'),
        'USER': os.getenv('MM_PROXY_DB_USER'),
        'PASSWORD': os.getenv('MM_PROXY_DB_PASSWORD'),
        'HOST': os.getenv('MM_PROXY_DB_HOST'),
        'PORT': '',
    }
}
```
Where the `MM_PROXY_DB_<>` are environmental variables which need to be set in the development environment, as well as in the staging or production servers. These variables contain the required parameters or credentials to access the database server.
